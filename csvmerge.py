#!/usr/bin/env python


"""Merge a group of CSV and TSV files into a single XLS workbook."""


__author__ = "Alex Cappiello"


import os
import sys
import glob
import argparse

from xlwt import *


def append_sheet(filename, newline="\n"):
    """Add a new sheet to the workbook from the specified file."""

    (name, ext) = os.path.basename(filename).split(".")

    print "opening: %s" % (filename)

    # Detect delimeter.
    if (ext == "csv"):
        delim = ","
    elif (ext == "tsv"):
        delim = "\t"
    else:
        sys.stderr.write("Unknown file extension: .%s\n" % (ext))
        return

    sheetname = name
    idx = 1
    # Break into the internals to prevent duplicate names.
    while (sheetname in w._Workbook__worksheet_idx_from_name):
        sheetname = "%s_%d" % (name, idx)
        idx += 1
    ws = w.add_sheet(sheetname)

    f = open(filename, "rt")

    raw = f.read()
    #print raw
    lines = raw.split("\n")

    # Iterate through each cell.
    for (i, line) in enumerate(lines):
        cells = line.split(delim)
        for (j, cell) in enumerate(cells):
            # Convert numbers so that they're formatted correctly.
            # Note that xlwt makes ints floats anyway.
            try:
                cell = float(cell)
            except:
                pass
            ws.write(i, j, cell)


def main():
    """Load args and process arguments sequentially."""

    msg = "Merge CSV/TSV files into a single xls spreadsheet."
    parser = argparse.ArgumentParser(description=msg)

    parser.add_argument("files", nargs="*", default=["*.csv"], \
                            help="Input files. DEFAULT: *.csv.")
    parser.add_argument("output", help="Output file name.")
    parser.add_argument("-n", "--newline", default="\n", \
                            help="Newline, i.e. '\\n' (DEFAULT) or '\\r\\n'.")

    args = parser.parse_args()

    # Create the workbook.
    global w
    w = Workbook()

    for f in args.files:
        # Use glob to allow for patterns in the input names.
        matches = glob.glob(f)

        if (len(matches) == 0):
            sys.stderr.write("file does not exist: %s\n" % (f))

        for m in matches:
            append_sheet(m, newline=args.newline)

    try:
        w.save(args.output)
        print "wrote output file: %s" % (args.output)
    except:
        sys.stderr.write("failed to write output file: %s\n" % (args.output))
        sys.exit(1)


if (__name__ == "__main__"):
    main()
